#include <stdlib.h>
#include <stdio.h>
#include "graph.h"
#include "road.h"
#include "list.h"
#include "town.h"

int main() {
  graph G = readmap ();

  viewmap ( G );

  freeGraph ( G );

  return EXIT_SUCCESS;
}
