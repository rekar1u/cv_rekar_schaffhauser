#include <stdlib.h>
#include <stdio.h>
#include "road.h"
#include "town.h"
#include <string.h>

struct road * createRoad (struct town * U, struct town * V) {
  struct road * R = (struct road *)calloc(1, sizeof(struct road));

  R->U = U;
  R->V = V;

  return R;
}

void freeRoad ( struct road * R ) {
  if (R->U == NULL && R->V == NULL)
    free(R);
}

struct town * getURoad(struct road * R) {
  return R->U;
}

void setURoad ( struct road * R, struct town * T ) {
  R->U = T;
}

struct town * getVRoad(struct road * R) {
  return R->V;
}

void setVRoad ( struct road * R, struct town * T ) {
  R->V = T;
}

void viewRoad ( struct road * R ) {
  printf("(%s, %s)\n", R->U->name, R->V->name);
}

struct list* getRoadListFromFile(char* filename) {//Construit une liste de routes � partir d'un fichier de routes
    struct list* roadList = new();
    struct list* townList = new();

    FILE* ptrF = fopen(filename, "r");
    if (ptrF) {
        int n;
        fscanf(ptrF, "%d\n", &n);//On initialise n au nombre de routes � traiter

        char nom1[50], nom2[50], * nomDyn1, * nomDyn2;
        struct town* town1, * town2;
        bool trouve1 = false, trouve2 = false;
        /*M�thode :
            - On scanne les 2 noms de villes
            - POur chaque ville, si elle existe d�j� on prends sa r�f�rence
            - Sinon on alloue son nom, puis on la cr�e et on l'ins�re dans la liste de villes qui sert de dico
          */

        for (int i = 0; i < n; i++) {//POur chaque route du fichier, on la scanne
            trouve1 = false; trouve2 = false;
            fscanf(ptrF, "( %s , %s )\n", nom1, nom2);

            struct elmlist* iterator = townList->head;

            while (iterator) {
                if (strcmp(nom1, getTownName(iterator->data)) == 0) {
                    trouve1 = true;
                    town1 = iterator->data;
                }
                if (strcmp(nom2, getTownName(iterator->data)) == 0) {
                    trouve2 = true;
                    town2 = iterator->data;
                }

                iterator = iterator->suc;
            }

            if (!trouve1) {
                nomDyn1 = (char*)calloc(strlen(nom1) + 1, sizeof(char));
                sprintf(nomDyn1, "%s", nom1);
                town1 = createTown(nomDyn1);
                cons(townList, town1);
            }
            if (!trouve2) {
                nomDyn2 = (char*)calloc(strlen(nom2) + 1, sizeof(char));
                sprintf(nomDyn2, "%s", nom2);
                town2 = createTown(nomDyn2);
                cons(townList, town2);
            }

            struct road* roadTemp = createRoad(town1, town2);
            cons(roadList, roadTemp);
        }


        dellist(townList, NULL);
        fclose(ptrF);
        return roadList;
    }
    else {
        printf("\nErreur, Fichier %s Non Ouvert!!\n", filename);
        return NULL;
    }
}

void dellRoadList(struct list* roadlist) {
    struct elmlist* iterator = roadlist->head;

    while (iterator) {
        struct town* U = getURoad(iterator->data);
        struct town* V = getVRoad(iterator->data);

        if (iterator->suc) {
            struct elmlist* iteratorTemp = iterator->suc;
            while (iteratorTemp) {
                if (getURoad(iteratorTemp->data) == U || getURoad(iteratorTemp->data) == V)
                    setURoad(iteratorTemp->data, NULL);
                if (getVRoad(iteratorTemp->data) == U || getVRoad(iteratorTemp->data) == V)
                    setVRoad(iteratorTemp->data, NULL);

                iteratorTemp = iteratorTemp->suc;
            }
        }

        if (getURoad(iterator->data) != NULL) freeTown(getURoad(iterator->data));
        if (getVRoad(iterator->data) != NULL) freeTown(getVRoad(iterator->data));

        free(iterator->data);
        iterator = iterator->suc;
    }

    dellist(roadlist, NULL);
}
